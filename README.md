# no-more-utm

no-more-utm removes any utm (Urchin Tracking Module) parameters from URLs passed to X clipboard.

## Dependencies

- [clipnotify](https://github.com/cdown/clipnotify)
- xclip or xsel
- bash
- make (optional)
- notify-send (optional)

## Installation

`sudo make install`

## Usage

Run `no-more-utm &` in your init script.

You can also have the script send notifications with the `-n` option
